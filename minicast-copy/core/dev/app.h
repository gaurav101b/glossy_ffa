/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \file
 *         App core, header file.
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#ifndef APP_H_
#define APP_H_

#include "node-id.h"
#include "contiki.h"
#include "dev/watchdog.h"
#include "dev/cc2420_const.h"
#include "cc2420.h"
#include "dev/leds.h"
#include "dev/spi.h"
#include <stdio.h>
#include <legacymsp430.h>
#include <stdlib.h>

/**
 * If not zero, nodes print additional debug information (disabled by default).
 */
#define APP_DEBUG 0
#define APP_DEB_PRINT 0

#define MAX_NEW 20
#define MAX_NEW_BIG 50 // must be larger than num of nodes

#define RECOVERY 0
#define RESTRICTION 0
#define QUALITY -60
#define IGNORE 1

/**
 * Initiator timeout, in number of Glossy slots.
 * When the timeout expires, if the initiator has not received any packet
 * after its first transmission it transmits again.
 */
#define APP_INITIATOR_TIMEOUT      3



// Struct to store the data of neighbours
typedef struct {
	uint8_t neighbour_node_id; /** The node id of the neighbour */
	uint8_t brodcast_from; /** Total number of packets this neighbour have brodcasted  */
	uint8_t recieved_from; /** The number of packets recieved from this node  */
	uint8_t last_rssi;
} neighbour_data_struct;

// Length of neighbour_data_struct
#define NEIGHBOUR_LEN                    sizeof(neighbour_data_struct)

//Maximum number of neighbours whose data can be stored
#define MAX_NUM_NEIGHBOURS	100

#define INIT_FLAG		255


/**
 * Ratio between the frequencies of the DCO and the low-frequency clocks
 */

#if COOJA
#define CLOCK_PHI                     (4194304uL / RTIMER_SECOND)
#else
#define CLOCK_PHI                     (F_CPU / RTIMER_SECOND)
#endif /* COOJA */

#define APP_HEADER                 0xa0
#define APP_HEADER_MASK            0xf0
#define APP_HEADER_LEN             sizeof(uint8_t)
#define APP_RELAY_CNT_LEN          sizeof(uint8_t)
#define APP_CHAIN_CNT_LEN          sizeof(uint8_t)
#define APP_IS_ON()                (get_app_state() != APP_STATE_OFF)
#define APP_FOOTER_LEN                    2
#define APP_FOOTER1_CRC_OK                0x80
#define APP_FOOTER1_CORRELATION           0x7f

#define APP_LEN_FIELD              app_packet[0]
#define APP_HEADER_FIELD           app_packet[1]
#define APP_CHAIN_CNT_FIELD        app_packet[2]
#define APP_NODE_ID_FIELD          app_packet[3]
#define APP_BRODCAT_CNT_FIELD      app_packet[4]
#define APP_RSSI_FIELD             app_packet[5]
#define APP_CRC_FIELD              app_packet[6]

#define APP_PACKET_LENGTH				6 

enum {
	APP_INITIATOR = 1, APP_RECEIVER = 0
};

/**
 * List of possible Glossy states.
 */
enum app_state {
	APP_STATE_OFF,          /**< Glossy is not executing */
	APP_STATE_WAITING,      /**< Glossy is waiting for a packet being flooded */
	APP_STATE_RECEIVING,    /**< Glossy is receiving a packet */
	APP_STATE_RECEIVED,     /**< Glossy has just finished receiving a packet */
	APP_STATE_TRANSMITTING, /**< Glossy is transmitting a packet */
	APP_STATE_TRANSMITTED,  /**< Glossy has just finished transmitting a packet */
	APP_STATE_ABORTED,      /**< Glossy has just aborted a packet reception */
	APP_STATE_RECOVERY		/**< Complicated */
};


void print_app_states(void);

PROCESS_NAME(app_process);

inline void state_machine_app(unsigned short app_tbiv);

/* ----------------------- Application interface -------------------- */
/**
 * \defgroup glossy_interface Glossy API
 * @{
 * \file   glossy.h
 * \file   glossy.c
 */

/**
 * \defgroup glossy_main Interface related to flooding
 * @{
 */

/**
 * \brief            Start Glossy and stall all other application tasks.
 *
 * \param data_      A pointer to the flooding data.
 *
 *                   At the initiator, Glossy reads from the given memory
 *                   location data provided by the application.
 *
 *                   At a receiver, Glossy writes to the given memory
 *                   location data for the application.
 * \param data_len_  Length of the flooding data, in bytes.
 * \param initiator_ Not zero if the node is the initiator,
 *                   zero if it is a receiver.
 * \param sync_      Not zero if Glossy must provide time synchronization,
 *                   zero otherwise.
 * \param tx_max_    Maximum number of transmissions (N).
 * \param header_    Application-specific header (value between 0x0 and 0xf).
 * \param t_stop_    Time instant at which Glossy must stop, in case it is
 *                   still running.
 * \param cb_        Callback function, called when Glossy terminates its
 *                   execution.
 * \param rtimer_    First argument of the callback function.
 * \param ptr_       Second argument of the callback function.
 */
void app_start(uint8_t app_initiator_, 

		 uint8_t app_header_, rtimer_clock_t app_t_stop_, rtimer_callback_t app_cb_,
		struct rtimer *app_rtimer_, void *app_ptr_);

/**
 * \brief            Stop Glossy and resume all other application tasks.
 * \returns          Number of times the packet has been received during
 *                   last Glossy phase.
 *                   If it is zero, the packet was not successfully received.
 * \sa               get_rx_cnt
 */
uint8_t app_stop(void);

/**
 * \brief            Get the last received counter.
 * \returns          Number of times the packet has been received during
 *                   last Glossy phase.
 *                   If it is zero, the packet was not successfully received.
 */
uint8_t get_app_rx_cnt(void);
uint8_t get_app_tx_cnt(void);
unsigned long get_rel_cnt(void);

/**
 * \brief            Get the current APP state.
 * \return           Current Glossy state, one of the possible values
 *                   of \link glossy_state \endlink.
 */

uint8_t get_app_brodcast_cnt(void);
uint8_t get_app_num_neighbours(void);
uint8_t get_app_init_cnt(void);
neighbour_data_struct * get_app_neighbours(void);

uint8_t get_app_state(void);
uint8_t get_app_rx_cnt_self(void);
uint8_t get_app_rx_cnt_other(void);
uint16_t get_app_grp_init(void);
void print_power_array(void);


/* ----------------------- Interrupt functions ---------------------- */
/**
 * \defgroup app_interrupts Interrupt functions
 * @{
 */
uint8_t app_header_curr,app_header_org,app_header_start,app_header_field_start,app_header_field_curr,bad_hdr_cnt;
inline void app_end_rx_or_tx(void);
inline void app_begin_rx(void);
inline void app_end_rx(void);
inline void app_begin_tx(void);
inline void app_end_tx(void);
/** @} */

/**
 * \defgroup glossy_capture Timer capture of clock ticks
 * @{
 */



/**
 * \brief            Get low-frequency time of first packet reception
 *                   during the last Glossy phase.
 * \returns          Low-frequency time of first packet reception
 *                   during the last Glossy phase.
 */
rtimer_clock_t get_app_t_first_rx_l(void);
rtimer_clock_t get_app_t_first_rx_self(void);
rtimer_clock_t get_app_t_start(void);

#endif /* APP_H_ */

/** @} */
