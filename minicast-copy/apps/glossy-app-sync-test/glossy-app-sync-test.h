/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \defgroup glossy-test Simple mintlication for testing Glossy
 * @{
 */

/**
 * \file
 *         A simple example of an mintlication that uses Glossy, header file.
 *
 *         The mintlication schedules Glossy periodically.
 *         The period is determined by \link GLOSSY_PERIOD \endlink.
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#ifndef GLOSSY_MINT_TEST_H_
#define GLOSSY_MINT_TEST_H_

#include "glossy.h"
#include "app.h"
#include "random.h"
#include "node-id.h"

/*------------------------------------------------------------------------------------*/
/**
 * \defgroup glossy-test-settings Application settings
 * @{
 */


/**
 * \brief NodeId of the initiator.
 *        Default value: 1
 */

#define ONLY_GLOSSY		9	// number of time glossy will run before scheduling mint for the first time 
#define RANDOM_SEED     4618

// Number of slots in which nodes can brodcast. A node will brodcast in a slot as per modulo of this value
#define NUM_SLOTS	25

#define RANDOM_INIT				0		//randomly choose INITIATOR every iteration? 1=yes 0=no
#define INIT_PROBABILITY		0.1f	//probability of any node of being initiator 

#define NUM_PRIME				3		//No. of unique prime values for num of slots
static uint8_t prime_array[NUM_PRIME] = {5,7,11};

//-----------------------------------------------------------------------------------------------------------------
#define NUM_OF_NODES	NUM_NODES

#if EXP_LOCATION == LOCATION_LOCAL_CUSTOM

#define NUM_NODES 10
static uint8_t initiator_ids[NUM_NODES] = {1,2,3,4,5,6,7,8,9,10};
//static uint8_t initiator_ids[NUM_NODES] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
//static uint8_t initiator_ids[NUM_NODES] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};
//static uint8_t initiator_ids[NUM_NODES] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40};
//static uint8_t initiator_ids[NUM_NODES] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50};
//static uint8_t initiator_ids[NUM_NODES] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100};

#elif EXP_LOCATION == LOCATION_DCUBE

#define NUM_NODES 28
#define NUM_OF_NODES	NUM_NODES
static uint8_t initiator_ids[NUM_NODES] = {224,203,209,217,218,223,216,215,202,201,200,220,100,103,113,109,101,106,107,112,104,213,108,116,210,110,115,118};

#endif

//------------------------------------------------------------------------------------------------------------------

#define INITIATOR_NODE_ID 		1
#define MINT_INITIATOR_NODE_ID  1//(initiator_ids[packetsync_iter])

/**
 * \brief Application-specific header.
 *        Default value: 0x0
 */
#define APPLICATION_HEADER      0

/**
 * \brief Maximum number of transmissions N.
 *        Default value: 5.
 */
#define N_TX                    10
#define MINT_N_TX               10
#define CHAIN_LENGTH            12

/* ------------------------------------------------------------------------ */
/**
 * \brief Period with which a Glossy phase is scheduled.
 *        Default value: 250 ms.
 */
#define GLOSSY_PERIOD 		(RTIMER_SECOND / 5)//(RTIMER_SECOND / 4)      // 250 ms

/**
 * \brief Duration of each Glossy phase.
 *        Default value: 20 ms.
 */
#define GLOSSY_DURATION 	(RTIMER_SECOND / 50)//50)     //  20 ms
#define MINT_DURATION		(RTIMER_SECOND / 200)
#define GAP (RTIMER_SECOND/128)	// This is the gap between the glossy
								// duration end and the start of the mint
								// can be a small value - but some operations
								// are there after the rtimer is scheduled
								// - it should be enough to execute that
								// example - estimate clock skew.

/**
 * \brief Guard-time at receivers.
 *        Default value: 526 us.
 */
#if COOJA
#define GLOSSY_GUARD_TIME 		(RTIMER_SECOND / 1000)
#else
#define GLOSSY_GUARD_TIME       (RTIMER_SECOND / 1900)   // 526 us
#endif /* COOJA */

#define APP_GUARD_TIME			(RTIMER_SECOND / 1024)
/**
 * \brief Number of consecutive Glossy phases with successful computation of reference time required to exit from bootstrminting.
 *        Default value: 3.
 */
#define GLOSSY_BOOTSTRAP_PERIODS 3

/**
 * \brief Period during bootstrminting at receivers.
 *        It should not be an exact fraction of \link GLOSSY_PERIOD \endlink.
 *        Default value: 69.474 ms.
 */
#define GLOSSY_INIT_PERIOD      (GLOSSY_INIT_DURATION + RTIMER_SECOND / 100)                   //  69.474 ms

/**
 * \brief Duration during bootstrminting at receivers.
 *        Default value: 59.474 ms.
 */

#define GLOSSY_INIT_DURATION    (GLOSSY_DURATION - GLOSSY_GUARD_TIME + GLOSSY_INIT_GUARD_TIME) //  59.474 ms

/**
 * \brief Guard-time during bootstrminting at receivers.
 *        Default value: 50 ms.
 */
#define GLOSSY_INIT_GUARD_TIME  (RTIMER_SECOND / 20)                                           //  50 ms

/**
 * \brief Data structure used to represent flooding data.
 */
typedef struct {
	unsigned long mint_seq_no; /**< Sequence number, incremented by the initiator at each Glossy phase. */
} mint_data_struct;

typedef struct {
	unsigned long seq_no; /**< Sequence number, incremented by the initiator at each Glossy phase. */
} glossy_data_struct;

/** @} */

/**
 * \defgroup glossy-test-defines Application internal defines
 * @{
 */

/**
 * \brief Length of data structure.
 */
#define DATA_LEN                    sizeof(glossy_data_struct)
#define MINT_DATA_LEN               sizeof(mint_data_struct)

/**
 * \brief Check if the nodeId matches the one of the initiator.
 */
#define IS_INITIATOR()              (node_id == INITIATOR_NODE_ID)
#define IS_MINT_INITIATOR()         (initiator_flag == 1)

/**
 * \brief Check if Glossy is still bootstrminting.
 * \sa \link GLOSSY_BOOTSTRAP_PERIODS \endlink.
 */
#define GLOSSY_IS_BOOTSTRAPPING()   (skew_estimated < GLOSSY_BOOTSTRAP_PERIODS)

/**
 * \brief Check if Glossy is synchronized.
 *
 * The mintlication assumes that a node is synchronized if it updated the reference time
 * during the last Glossy phase.
 * \sa \link is_t_ref_l_updated \endlink
 */
#define GLOSSY_IS_SYNCED()          (is_t_ref_l_updated())

/**
 * \brief Get Glossy reference time.
 * \sa \link get_t_ref_l \endlink
 */
#define GLOSSY_REFERENCE_TIME       (get_t_ref_l())

/** @} */

/** @} */

#endif /* GLOSSY_TEST_H_ */


