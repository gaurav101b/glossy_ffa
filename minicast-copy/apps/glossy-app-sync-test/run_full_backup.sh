#!/bin/bash

sudo -s

num_nodes="6"
data_size=(1)
max_power=(4 8 15 31)
config=("CONFIG_FIXED")
# virual_slots=(10 20 40 80)
#schedule_sizes=(10 20 40)

rm -rf results/
mkdir results

for power in "${max_power[@]}"
do

	sed -i "31s/.*/#define MAX_POWER_LEVEL $power/" project-conf.h

	for size in "${data_size[@]}"
	do
		sed -i "30s/.*/#define DATA_SIZE $size/" project-conf.h

		for setting in "${config[@]}"
		do
			sed -i "169s/.*/#define CONFIG $setting/" packetsync-test.h

			if [ "$setting" == "CONFIG_VIRTUAL" ]; then
				for slots in "${virual_slots[@]}"
				do
					sed -i "181s/.*/#define VIRTUAL_SLOTS $slots/" packetsync-test.h
					# got the required config
					echo "Running data_size:$size config:$setting slots:$slots "
					sudo python run_exp.py $num_nodes log-$initiator-$num_slots-$data_size				
				done
			elif [ "$setting" == "CONFIG_SCHEDULE" ]; then
				for slots in "${schedule_sizes[@]}"
				do
					sed -i "187s/.*/#define SIZE_OF_SCHEDULE $size/" packetsync-test.h
					# got the required config
					echo "Running data_size:$size config:$setting slots:$slots "
					sudo python run_exp.py $num_nodes log-$initiator-$num_slots-$data_size				
				done
			else
				# got the required config
				echo "Running data_size:$size config:$setting slots:$slots "
				sudo python run_exp.py $num_nodes log-$initiator-$num_slots-$data_size
			fi
		done
	done

done
